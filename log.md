# 100 Days Of Fitness - Log

Recently, I've started the #100DaysOfCode challenge and it's been
recommended to me to take upon a complimentary challenge with it. I am quite
physically active, so I think starting the Fitness challenge shouldn't be that
difficult. I do Kenpo an ancient martial art from Okinawa. I have trainings 
three times a week. These three days are pretty intensive as I tend to do a gym
based exercise (running on a machine or pumping iron) for a 40-60 minutes before
the Kenpo class and then I usually stay for the boxing class afterwards.

My plan is to add another three days of relaxing activities such dog walks or
bicycle or stationary bicycle and I'd like to get back into running on Sunday
mornings with our running group here in Vélez de Benaudalla.

## Day 12: July 19, 2019

**Today's Activity:** Kenpo – helping the younger students to prepare them for
the 1st kyu exam.

**Thoughts**: It's hot, I mean real hot, mid-summer hot, so the sensei is taking
the Fridays easy. I was helping Miguel and Helena with their self-defence 
techniques and their description and explanation.

## Day 11: July 18, 2019

**Today's Activity:**
* 10 mins warm up run on treadmill; 
* 25 mins back muscles exercise - 3x20 repetions of each exercise:
  1) 20kg lat pulldown to chest (polea alta al pecho)
  2) 20kg long pulley row (Remo polea baja)
  3) 20kg (Pullover en Polea Alta con Cuerda)
  4) 20kg (Remo Lumbar Polea baja con cuerda)
  5) 1kg bending forward
* 60 mins Kenpo (blue belt self-defense techniques)
* 5 mins skipping rope
* 15 mins abdominal muscles exercise
* 20 mins exercises to improve positioning in fight 
* 15 mins punch bag (1 min on / 1 min off)
* stretching

**Thoughts:** During boxing, I was feeling pain on the end of the 3rd or 4th 
metatarsal of my left foot. It's the same spot I had problems with in the past. 
I must focus on correcting my technique. I reckon it's the way I step as it 
hurts when I'm landing on the foot while jumping back off the front foot after 
throwing a punch.

## Day 10: July 17, 2019

**Today's Activity:** Walking

**Thoughts:** Regeneration day. Easy walk following Emma's favourite running 
track. It's nice and cool down by the river. Strava got a bit creative with our 
GPS tracking towards the end, so there is very little value in terms of stats. 
[Strava activity](https://t.co/W6gbXg5SxD)

## Day 9: July 16, 2019

**Today's Activity:** 
* 5 mins warm up run on treadmill; 
* 20 mins back muscles exercise - 3x20 repetions of each exercise:
  1) 20kg lat pulldown to chest (polea alta al pecho)
  2) 20kg long pulley row (Remo polea baja)
  3) 20kg (Pullover en Polea Alta con Cuerda)
  4) 20kg (Remo Lumbar Polea baja con cuerda)
  5) 1kg bending forward
* 60 mins Kenpo (blue belt self-defense techniques)
* 10 mins workout with exercise ball (various exercises for belly muscles and/or
balance)
* 5 mins skipping rope
* 35 mins basic boxing techniques
* 10 mins light sparring

**Thoughts:* It's been a long day, I'm rushing to get to bed. I feel I gotta cut
some corners. Today's log entry is one of them.

## Day 8: July 15, 2019

**Today's Activity:** Walking

**Thoughts:** A regeneration day. I feel the run yesterday stretched me a bit, 
so taking it easy. I also had a pretty full on day in the office again. So we 
went for a walk. I set off a bit grumpy, but walking got rid off that mindset 
pretty fast. [Strava activity](https://www.strava.com/activities/2535162326)

## Day 7: July 14, 2019

**Today's Activity:** Sunday run with my running buddies

**Thoughts:** Well, actually – with a running buddy, Juan, the rest of the group
couldn't make it today. It was my first run since March, thus we took an easy 
route. We ran along the river Guadalfeo from Joaquin's greenhouse to the big dam
and back. All together about 8.6km in about 55 mins. The main focus was – _Don't
over do it, silly!_ More details on my [Strava activity](https://www.strava.com/activities/2530246341).

## Day 6: July 13, 2019

**Today's Activity:** Morning stroll with our dog.

**Thoughts:** Today has been planned as a relaxing day and even though the last 
night's class hasn't been very taxing, I decided to go with it. Tomorrow, I'm 
going for a run with my running buddies. Something, I have not really done since 
last year when I did my back in. Anyway, that's tomorrow. Let's stay focused on 
today. I went in the morning leaving around 9.30am and it was already a bit too 
late. I was sweating and quite thirsty on the way back. I have to get my head 
around the fact, it is actually summer and I live in the south of Spain. Next 
time, I'll take water for me and for our four-legged friend.

You can see the way we went on my [Strava activity](https://www.strava.com/activities/2527593235).

## Day 5: July 12, 2019

**Today's Activity:** Kenpo – helping the younger student to prepare them for
the 1st kyu exam.

**Thoughts:** A relaxing class, this is was the first time in many years that I 
came home from training wearing my kenpogi as I didn't sweat enough to force me 
to get changed after the class. I guess there are times to sweat and there are 
times to do other things. In the end it all adds up.

## Day 4: July 11, 2019

**Today's Activity:** 
* 30 mins run on treadmill; 
* 30 mins back muscles exercise - 3x20 repetions of each exercise:
  1) 20kg lat pulldown to chest (polea alta al pecho)
  2) 20kg long pulley row (Remo polea baja)
  3) 20kg (Pullover en Polea Alta con Cuerda)
  4) 20kg (Remo Lumbar Polea baja con cuerda)
  5) 1kg bending forward
* 60 mins Kenpo (Saro Buto Shodan – bunkai)
* 55 mins boxing exercises aimed at improving endurance
* 5 mins stretching

**Thoughts:** I'm getting used the half an hour easy run at the beginning. The 
worst part is actually to keep my mind occupied during the run. If I focus too 
much on all the numbers on the display of the treadmill, it wears me down. Today
I had Sergio and our sensei talking to me about the reunion of our Kenpo alumni 
that is planned for the end of the month. With my brain occupied, 30 minutes 
were over in no time.

Then I did my usual routine for strengthening my shoulder, core and back 
muscles. I had more time on my hands, thus I put in practice all the advice I 
got of various Youtube videos the other day.

Kenpo – we've got a bunch of younger guys in our class who are getting ready for 
the 1st kyu exam, so today we were recapping the bunkai (application of 
techniques) for the first kata of the Monkey's set (Saro Buto Shodan). I was 
working with Laura (2nd dan) and Joaquin (1 dan) and we were having a laugh, 
making sounds like from the old kung-fu movies.

Boxing – that was a proper cardio workout. 
* We started off gently with some skipping the rope, then a bit of shadow work 
in front of the mirror. 
* Next, sensei split us into pairs and we were taking turns in the following 
exercise. Using the Kenpo belts and carabines, you click yourself onto a bunch 
of elastic ropes and then you run in various directions as far as the elastic 
ropes allow you and touch the floor. The other person in the meantime is shadow 
boxing with a pair of 1kg weights (slowly, not stretching much the joins to 
avoid injuries). And after a period you swap.
* We continued the class with another elastic bands exercise. This time, the 
one, that is resting, is holding one end of the elastic bands and the other one 
is boxing the punch bag. The one holding is creating resistance, pulling the 
other one away from the bag.

## Day 3: July 10, 2019

**Today's Activity:** Today, we went for a stroll. 10.1 km in 2:45 Hrs

**Thoughts:** I really enjoy being in nature. A few months ago I pointed out a 
track to Emma, where I sometimes go running with the lads on Sundays, saying 
that it's really pretty. She remembered and asked me to show her. It starts 
about three kilometres out of Vélez on the old road to Motril. It took us about 
an hour to get there whilst we had to keep Sugar (our dog) on the lead as the 
people drive past relatively fast and the road is full of curves, where a car 
just suddenly appears from behind a bend.

I had a hard day in the office, so I was a bit grumpy when we set off but being
in nature helped me get over it.

You can see the way we went on my [Strava activity](https://www.strava.com/activities/2521538210)

## Day 2: July 9, 2019

**Today's Activity:** 
* 30 mins run on treadmill; 
* 15 mins back muscles exercise - 3x20 repetions of each exercise:
  1) 20kg lat pulldown to chest (polea alta al pecho)
  2) 20kg long pulley row (Remo polea baja)
  3) 20kg (Pullover en Polea Alta con Cuerda)
  4) 20kg (Remo Lumbar Polea baja con cuerda)
  5) 1kg bending forward
* 60 mins Kenpo (purple belt self-defense techniques)
* 40 mins workout with exercise ball (various exercises for belly muscles and/or
balance)
* 20 mins stretching

**Thoughts:* I'm used to training three times a week, doing Kenpo and boxing. 
This can be pretty intense, but recently I have been forced to have a prolonged
break due to a chain of injuries of various back muscles. This stopped me from
training for almost 8-10 months and it still is my weak spot. My usual pattern 
is to overdo it at the beginning and end-up unable to continue with my 
challenges. Therefore I'm being really careful and practicing my patience. 
Today's routine was long but actually not as intense as it could be.

## Day 1: July 8, 2019

**Today's Activity:** Walk with my wife and our dog

**Thoughts:** First day of the challenge. I'm well up for it. It was supposed to
be my relaxing day, so I kept reminding myself, that it's ok to stop and enjoy 
the view, take a photo or let my wife to splash around in the reservoir that was
the destination of our walk. The details are on [Strava](https://www.strava.com/activities/2515643772).
